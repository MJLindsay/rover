package com.citi.rover;

public class Rover {
    

    //Is location and position different?
    private int X;
    private int Y;
    // int stepsToGo = 2;
    private StringBuilder nextSteps = new StringBuilder();
    //0==N, 1==E, 2==S, 3==W
    private int direction;

    public void receiveCommands(String command){
        //loop through the string
        for(int i = 0; i <command.length(); i++){
            char c = command.charAt(i);
            //grab the first char
            //check if it is a number
            if(Character.isDigit(c)==true){
                int temp = Character.getNumericValue(c);
                //loop the size of the number adding the 'M' char to the nextSteps 
                for(int j = 0; j <temp; j++){
                    this.nextSteps.append('M');
                }
            }
            else {
                this.nextSteps.append(c);
            }

        }
        System.out.println("receiveCommands called");

    }
   
    public void takeNextStep(){
        if(this.nextSteps.length() ==0){
            //Fixes error when there are no commands left (for both TestProgram2 and TestUI)!!
            System.out.println("Awaiting more commands!!");
        }
        else{
       char in = this.nextSteps.charAt(0);
       //Removes parsed command
       this.nextSteps = this.nextSteps.deleteCharAt(0);

       switch (this.direction) {
            case 0:
            //facing North
            if (in == 'R'){
                this.setDirection(1);
            }
            else if(in =='L'){
                this.setDirection(3);
            }
            else {
                this.setY(this.getY()+1);
            }
               break;

            case 1:
            //facing East
            if (in == 'R'){
                this.setDirection(2);
            }
            else if(in =='L'){
                this.setDirection(0);
            }
            else {
                this.setX(this.getX()+1);
            }
               break;

            case 2:
            //facing South
            if (in == 'R'){
                this.setDirection(3);
            }
            else if(in =='L'){
                this.setDirection(1);
            }
            else {
                this.setY(this.getY()-1);
            }
               break;

            case 3:
            //facing West
            if (in == 'R'){
                this.setDirection(0);
            }
            else if(in =='L'){
                this.setDirection(2);
            }
            else {
                this.setX(this.getX()-1);
            }
               break;
       
           default:
               break;
       }
    }

    }

    //I have made this return an int because the TestUI did not like a String return :)
    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        this.X = x;
    }

    public int getY() {
        return Y;
    }

    public void setY(int y) {
        this.Y = y;
    }     

    public boolean isBusy() {
        if(this.nextSteps.length()!=0){
            return true;
        }
        else{
            return false;
        }
    }


}